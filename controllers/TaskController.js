const Task = require('../models/Task.js')


// get all---------------------------
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// post----------------------------------

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if (error) {
			return error
		}

		return 'Task Created successfuly'
	})
}

// update--------------------------------------------
module.exports.completeTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,error) => {
		if (error) {
			return error
		}

		result.status = newContent.status
		return result.save().then((completedTask, error) => {
			if(error) {
				return error
			}

			return completedTask
		})
	})
}

// retrieve task------------------------------------------
module.exports.retrieveTask = (taskId) => {
	return Task.findById(taskId).then((retrievedTask, error) => {
		if(error){
			return error
		}
		return retrievedTask
	})
}