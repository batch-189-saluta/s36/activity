const express = require('express')
const TaskController = require('../controllers/TaskController.js')

const router = express.Router()

// get router---------------------------------

router.get('/', (request, response) => {
	// Business logic here

	TaskController.getAllTasks().then((tasks) => response.send(tasks))

})

// post router-----------------------------------

router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task)=>response.send(task))
})


// update router----------------------------------------

router.put('/:id/complete', (request,response) => {
	TaskController.completeTask(request.params.id, request.body).then((completedTask) => response.send(completedTask))
})

// retrieving----------------------------------------------

router.get('/:id/retrieve', (request, response) => {
	TaskController.retrieveTask(request.params.id).then((retrievedTask) => response.send(retrievedTask))
})






// call---------------------------
module.exports = router